package org.example;

public class App


{

    static User A;
    static double interValA;
    static double keyA;
    static User B;
    static double interValB;
    static double keyB;


    public static void main( String[] args )
    {


        A = new User(17, 12, 88);
        B = new User(17, 12, 104);

        interValA = calcInterVal(A.getBase(), A.getpNr(), A.getSecret());
        interValB = calcInterVal(B.getBase(), B.getpNr(), B.getSecret());
        System.out.println("A inter val: " + interValA);
        System.out.println("B inter val: " + interValB);

        keyA = calcKey(17, 100, interValB);
        keyB = calcKey(A.getpNr(), B.getSecret(), interValA);
        System.out.println("A key: " + keyA);
        System.out.println("B key: " + keyB);
    }

    public static double calcKey(double pNr, double secret, double inter) {
        return Math.floorMod((int) Math.pow(inter, secret), (int) pNr);
    }

    public static double calcInterVal(double base, double pNr, double secret) {
        return Math.floorMod((int) Math.pow(base, secret), (int) pNr);
    }
}
