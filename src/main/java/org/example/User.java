package org.example;

class User {
    double pNr;
    double base;
    double secret;

    public User(double pNr, double base, double secret) {
        this.pNr = pNr;
        this.base = base;
        this.secret = secret;
    }

    public double getpNr() {
        return pNr;
    }

    public void setpNr(double pNr) {
        this.pNr = pNr;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getSecret() {
        return secret;
    }

    public void setSecret(double secret) {
        this.secret = secret;
    }
}